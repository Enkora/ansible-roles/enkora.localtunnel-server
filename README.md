This role will install localtunnel on target server and run some prep-task:

Pre-requirements:

1. This role will require `docker` pip modules
2. As always, keep your hashivault tokens fresh and tasty.
3. Override variables specified in `defaults/main.yml` in your playbook, if required.

Playbook example:

```ansible
---
- hosts: localtunnel

  tasks:
    - include_role:
        name: enkora.localtunnel-server
```
